#!/bin/sh

docker run -d -h db --name ttme2_db --network ttme2 -p 5432:5432 -e POSTGRES_PASSWORD=Test1234 postgres:12.0-alpine
docker run -d -h cache --name ttme2_cache --network ttme2 redis
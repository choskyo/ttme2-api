#!/bin/sh

go mod vendor
docker build -t ttme2-api .
docker rm --force ttme2
docker run -d -h api -p 8080:8080 --name ttme2 --network ttme2 ttme2-api:latest
docker start ttme2
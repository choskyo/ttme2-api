### Todo

More of a nice-to-have list.

- Websocket operations need to use redis pubsub.
- Multiple image storage location options. (fs obviously not prod-suitable)
- Actually use pg db

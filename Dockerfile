FROM golang:alpine

ENV GO111MODULE=on

WORKDIR /app

COPY . .

RUN go build -mod vendor -o ttme2

EXPOSE 8080

ENV TTME2_REDIS_HOST="ttme2_cache:6379"
ENV TTME2_DB="host=ttme2_db port=5432 user=postgres password=Test1234 dbname=ttme2 sslmode=disable"

ENTRYPOINT ["/app/ttme2"]
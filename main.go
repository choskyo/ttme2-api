package main

import (
	"ttme2/cache"
	"ttme2/images"
	"ttme2/ws"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	c := cors.DefaultConfig()
	c.AllowOrigins = []string{"*"}
	c.AddAllowHeaders("Content-Disposition", "TTME-Sender", "TTME-Conversation")

	router.Use(cors.New(c))

	router.NoRoute(func(c *gin.Context) {
		c.Status(404)
	})

	cache.OpenClient()

	images.Initialise(router)

	ws.Initialise(router)

	router.Run(":8080")
}

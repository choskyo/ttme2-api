package images

import (
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"ttme2/ws"

	"github.com/gin-gonic/gin"
)

// Initialise registers endpoints + ensures image storage loc exists
func Initialise(r *gin.Engine) {
	svc := &imageService{}

	os.Mkdir(imageDir, 0700)

	g := r.Group("/images/v1/")

	g.POST("upload", handleUploadEndpoint(svc))
	g.GET(":id", handleGetEndpoint(svc))
}

func handleUploadEndpoint(svc ImageService) gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.GetHeader("TTME-Sender") == "" || c.GetHeader("TTME-Conversation") == "" {
			c.JSON(http.StatusBadRequest, gin.H{"err": "Missing Sender or Conversation header."})
			return
		}

		f, h, err := c.Request.FormFile("f")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}
		defer f.Close()

		fileNameParts := strings.Split(h.Filename, ".")
		fileType := fileNameParts[len(fileNameParts)-1]

		b, err := ioutil.ReadAll(f)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		imageID, err := svc.SaveImage(b, fileType)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		ws.BroadcastImage(c.GetHeader("TTME-Conversation"), c.GetHeader("TTME-Sender"), imageID)

		c.Status(200)
	}
}

func handleGetEndpoint(svc ImageService) gin.HandlerFunc {
	return func(c *gin.Context) {
		fileName := c.Param("id")
		if strings.Contains(fileName, "..") || strings.Contains(fileName, " ") {
			c.Status(http.StatusOK)
			return
		}

		fileNameParts := strings.Split(fileName, ".")
		fileType := fileNameParts[len(fileNameParts)-1]

		img, err := svc.GetImage(fileName)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err})
			return
		}

		c.Header("Content-Disposition", "attachment; filename=\""+fileName+"\"")

		c.Data(http.StatusOK, "image/"+fileType, img)
	}
}

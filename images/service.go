package images

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"

	uuid "github.com/satori/go.uuid"
)

var homeDir, _ = os.UserHomeDir()
var imageDir = homeDir + "/ttme2-images"

// ImageService ...
type ImageService interface {
	SaveImage(image []byte, fileType string) (fileName string, err error)
	GetImage(imageName string) ([]byte, error)
}

type imageService struct {
}

func (s *imageService) SaveImage(image []byte, fileType string) (string, error) {
	imageID := uuid.NewV4()

	imgFile, err := os.Create(imageDir + "/" + imageID.String() + "." + fileType)
	if err != nil {
		return "", err
	}
	defer imgFile.Close()

	_, err = io.Copy(imgFile, bytes.NewReader(image))
	if err != nil {
		return "", err
	}

	return imageID.String() + "." + fileType, nil
}

func (s *imageService) GetImage(imageName string) ([]byte, error) {
	img, err := ioutil.ReadFile(imageDir + "/" + imageName)
	if err != nil {
		return []byte{}, err
	}

	return img, nil
}

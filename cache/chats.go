package cache

import uuid "github.com/satori/go.uuid"

// NewChat ...
func NewChat(userOne, userTwo uuid.UUID) uuid.UUID {
	newChatID := uuid.NewV4()

	cacheClient.HSet(newChatID.String(), map[string]interface{}{
		"user1": userOne.String(),
		"user2": userTwo.String(),
	})

	return newChatID
}

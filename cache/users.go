package cache

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
)

// NewUser ...
func NewUser(sessionID uuid.UUID) {
	_, err := cacheClient.HSet("users:"+sessionID.String(), map[string]interface{}{
		"ID":             sessionID.String(),
		"State":          "Waiting",
		"Name":           "",
		"ConversationID": uuid.Nil.String(),
	}).Result()

	if err != nil {
		fmt.Println("err setting new user")
		fmt.Println(err)
	}
}

// GetUser ...
func GetUser(sessionID uuid.UUID) map[string]string {
	var user map[string]string

	if u, err := cacheClient.HGetAll("users:" + sessionID.String()).Result(); err != nil {
		fmt.Println("GetUser: " + err.Error())
		user = make(map[string]string)
	} else {
		user = u
	}

	return user
}

// GetCurrentConversation ...
func GetCurrentConversation(sessionID uuid.UUID) uuid.UUID {
	res, _ := cacheClient.HGet("users:"+sessionID.String(), "ConversationID").Result()

	return uuid.FromStringOrNil(res)
}

// SetCurrentConversation ...
func SetCurrentConversation(conversationID, sessionID uuid.UUID) uuid.UUID {
	res := cacheClient.HSet("users:"+sessionID.String(),
		map[string]interface{}{"ConversationID": conversationID.String()})

	return uuid.FromStringOrNil(res.String())
}

// RemoveUser ...
func RemoveUser(sessionID uuid.UUID) {
	cacheClient.Del("users:" + sessionID.String())
}

// SetUserName ...
func SetUserName(sessionID uuid.UUID, newName string) {
	cacheClient.HSet("users:"+sessionID.String(), "Name", newName)
}

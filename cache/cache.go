package cache

import (
	"os"

	redis "github.com/go-redis/redis/v7"
)

var cacheClient *redis.Client
var pubSub *redis.PubSub

// OpenClient ...
func OpenClient() {
	client := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("TTME2_REDIS_HOST"),
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := client.Ping().Result()

	if err != nil {
		panic(err)
	}

	// TODO: Replace with clear on server err that only kills
	// cache entries belonging to err'd service instance
	client.FlushAll()

	cacheClient = client

	pubSub = cacheClient.Subscribe("chat")
}

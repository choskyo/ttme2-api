package cache

import (
	uuid "github.com/satori/go.uuid"
)

// QueueUser ...
func QueueUser(sessionID uuid.UUID) {
	cacheClient.RPush("queue", sessionID.String())
}

// UnqueueUser ...
func UnqueueUser(sessionID uuid.UUID) {
	cacheClient.LRem("queue", 1, sessionID.String())
}

// GetQueueLength ...
func GetQueueLength() int {
	res := int(cacheClient.LLen("queue").Val())

	return res
}

// GetFirstPair returns first 2 users in queue
func GetFirstPair() (user1 uuid.UUID, user2 uuid.UUID) {
	user1 = uuid.FromStringOrNil(cacheClient.LIndex("queue", 0).Val())
	user2 = uuid.FromStringOrNil(cacheClient.LIndex("queue", 1).Val())

	return user1, user2
}

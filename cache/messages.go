package cache

import (
	"encoding/json"
	"ttme2/repo"

	uuid "github.com/satori/go.uuid"
)

// PushMessage add message to chat history
func PushMessage(chatID uuid.UUID, message repo.Message) {
	o, err := json.Marshal(message)
	if err != nil {
		panic(err.Error())
	}

	if err := cacheClient.LPush("messages:"+chatID.String(), string(o)).Err(); err != nil {
		panic(err.Error())
	}
}

module ttme2

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/lib/pq v1.3.0
	github.com/satori/go.uuid v1.2.0
	gopkg.in/olahol/melody.v1 v1.0.0-20170518105555-d52139073376
)

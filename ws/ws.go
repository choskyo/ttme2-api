package ws

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
	"ttme2/cache"
	"ttme2/repo"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"gopkg.in/olahol/melody.v1"
)

var mdy *melody.Melody

// Initialise ...
func Initialise(router *gin.Engine) {
	mdy = melody.New()

	router.GET("/ws/v1", func(c *gin.Context) {
		mdy.HandleRequest(c.Writer, c.Request)
	})

	mdy.HandleConnect(func(s *melody.Session) {
		sessionID := uuid.NewV4()
		s.Keys = make(map[string]interface{})

		s.Keys["ID"] = sessionID.String()

		cache.NewUser(sessionID)

		mdy.BroadcastFilter([]byte("init:"+sessionID.String()), func(q *melody.Session) bool {
			return q.Keys["ID"] == s.Keys["ID"]
		})
	})

	mdy.HandleDisconnect(func(s *melody.Session) {
		sessionID := uuid.FromStringOrNil(s.Keys["ID"].(string))
		cache.RemoveUser(sessionID)
		cache.UnqueueUser(sessionID)
	})

	mdy.HandleMessage(handleMessage)

	ticker := time.NewTicker(1 * time.Second)

	go func() {
		for {
			select {
			case <-ticker.C:
				queueCount := strconv.Itoa(cache.GetQueueLength())
				mdy.Broadcast([]byte("queuecount:" + queueCount))
			}
		}
	}()

	queuePairTicker := time.NewTicker(50 * time.Millisecond)

	go func() {
		for {
			select {
			case <-queuePairTicker.C:
				if cache.GetQueueLength() < 2 {
					break
				}

				u1, u2 := cache.GetFirstPair()

				newConversationID := uuid.NewV4()

				cache.SetCurrentConversation(newConversationID, u1)
				cache.SetCurrentConversation(newConversationID, u2)

				cache.UnqueueUser(u1)
				cache.UnqueueUser(u2)

				mdy.BroadcastFilter(([]byte("chatStarted:" + newConversationID.String() + ":" + cache.GetUser(u1)["Name"])), func(q *melody.Session) bool {
					return u2.String() == q.Keys["ID"]
				})
				mdy.BroadcastFilter(([]byte("chatStarted:" + newConversationID.String() + ":" + cache.GetUser(u2)["Name"])), func(q *melody.Session) bool {
					return u1.String() == q.Keys["ID"]
				})
			}
		}
	}()
}

func handleMessage(s *melody.Session, rawMessage []byte) {
	message := string(rawMessage)
	messageParts := strings.SplitN(message, ":", 3)
	messageType, messageContent := messageParts[0], messageParts[2]

	sessionID := uuid.FromStringOrNil(s.Keys["ID"].(string))

	//TODO: per-case input validation

	fmt.Println(messageParts)

	switch messageType {
	case "queue":
		{
			cache.SetUserName(sessionID, messageContent)
			delete(s.Keys, "ConversationID")
			cache.QueueUser(sessionID)
		}
	case "unqueue":
		{
			cache.UnqueueUser(sessionID)
		}
	case "chat":
		{
			if s.Keys["ConversationID"] == uuid.Nil.String() {
				break
			}

			fmt.Println(cache.GetUser(sessionID))

			res := repo.Message{
				ID:       uuid.NewV4(),
				Content:  messageContent,
				Sender:   cache.GetUser(sessionID)["Name"],
				SentTime: time.Now(),
			}

			resJSON, _ := json.Marshal(res)

			mdy.BroadcastFilter([]byte("chat:"+string(resJSON)), func(q *melody.Session) bool {
				return s.Keys["ConversationID"] == q.Keys["ConversationID"]
			})
		}
	case "setname":
		{
			cache.SetUserName(sessionID, messageContent)

			if s.Keys["ConversationID"] != nil {
				mdy.BroadcastFilter([]byte("newname:"+messageContent), func(q *melody.Session) bool {
					return s.Keys["ConversationID"] == q.Keys["ConversationID"]
				})
			}
		}
	}
}

// BroadcastImage sends uploaded image to users in conversation.
func BroadcastImage(conversationID, senderID, imageURL string) {
	fmt.Println(cache.GetUser(uuid.FromStringOrNil(senderID)))

	res := repo.ImageMessage{
		ID:       uuid.NewV4(),
		Content:  imageURL,
		Sender:   cache.GetUser(uuid.FromStringOrNil(senderID))["Name"],
		SentTime: time.Now(),
		IsImage:  true,
	}

	fmt.Println(senderID)

	resJSON, _ := json.Marshal(res)

	mdy.BroadcastFilter([]byte("chat:"+string(resJSON)), func(q *melody.Session) bool {
		return sessionInConversation(conversationID, q)
	})
}

func sessionInConversation(conversationID string, q *melody.Session) bool {
	qConversationID := cache.GetCurrentConversation(uuid.FromStringOrNil(q.Keys["ID"].(string)))

	return qConversationID.String() == conversationID
}

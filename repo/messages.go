package repo

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

const (
	messagesTableName = "messages"
)

// Message type
type Message struct {
	ID       uuid.UUID `json:"id"`
	Sender   string    `json:"sender"`
	SentTime time.Time `json:"sentTime"`
	Content  string    `json:"content"`
}

// ImageMessage images uploaded as data urls
type ImageMessage struct {
	ID       uuid.UUID `json:"id"`
	Sender   string    `json:"sender"`
	SentTime time.Time `json:"sentTime"`
	Content  string    `json:"content"`
	IsImage  bool      `json:"isImage"`
}

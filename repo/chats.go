package repo

const (
	chatsTableName      = "chats"
	chatsIDCol          = "id"
	chatsTimeStartedCol = "time_started"
	chatsTimeEndedCol   = "time_ended"
)

package repo

import (
	"database/sql"
	"os"

	_ "github.com/lib/pq" // db driver
)

var dbPool *sql.DB

// OpenConnection ...
func OpenConnection() {
	conn, err := sql.Open("postgres", os.Getenv("TTME2_DB"))
	if err != nil {
		panic(err)
	}

	if err := conn.Ping(); err != nil {
		panic(err)
	}

	dbPool = conn
}
